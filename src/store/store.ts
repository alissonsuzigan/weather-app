import { createContext, ReactNode } from 'react';
import getWeatherByTerm from '../assets/scripts/api';

// Types ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
export interface IData {
  [prop: string]: any
};

export interface IAction {
  type: 'LOAD_WEATHER_REQUEST' | 'LOAD_WEATHER_SUCCESS' | 'LOAD_WEATHER_ERROR',
  payload?: {
    data?: IData,
    term: string
  }
};

export interface IState {
  data: IData,
  feedback: string,
  isLoading: boolean,
  term: string
}

export interface IContextProp {
  dispatch?: (fn: IAction) => void,
  state?: IState,
  children?: ReactNode,
  [otherProps: string]: any
};

// Action types :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
export const LOAD_WEATHER_REQUEST = 'LOAD_WEATHER_REQUEST';
export const LOAD_WEATHER_SUCCESS = 'LOAD_WEATHER_SUCCESS';
export const LOAD_WEATHER_ERROR = 'LOAD_WEATHER_ERROR';

// Action creators ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
export const showLoading = (): IAction => ({
  type: LOAD_WEATHER_REQUEST
});

export const loadWeatherByTerm = async (term: string): Promise<IAction> => {
  try {
    const response = await getWeatherByTerm(term);
    const data = await response.json();

    return {
      type: LOAD_WEATHER_SUCCESS,
      payload: { data, term }
    }
  }
  catch(error) {
    return {
      type: LOAD_WEATHER_ERROR,
      payload: { term }
    }
  }
}

// Initial application state
export const INITIAL_STATE: IState = {
  data: {},
  feedback: '',
  isLoading: false,
  term: ''
};

// Reducer
export const reducer = (state: IState = INITIAL_STATE, action: IAction) => {
  switch (action.type) {
    case LOAD_WEATHER_REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case LOAD_WEATHER_SUCCESS:
      const { data, term } = action.payload!;
      console.log(data, term);

      return {
        ...state,
        data: { ...state.data, [data!.name.toLowerCase()]: data },
        isLoading: false,
        term
      }

    // default:
    //   return state;
  }
};

// Context
export const Context = createContext({});