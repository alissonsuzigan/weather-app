import {
  showLoading, loadWeatherByTerm,
  LOAD_WEATHER_REQUEST, LOAD_WEATHER_SUCCESS, LOAD_WEATHER_ERROR,
  reducer
} from './store';

// Action creators
describe('Action creators', () => {
  describe('showLoading()', () => {
    it('returns an object with a type `LOAD_WEATHER_REQUEST`', () => {
      expect(showLoading()).toEqual({ type: LOAD_WEATHER_REQUEST });
    });
  });

  describe('loadWeatherByTerm()', () => {
    it('returns success with the weather data from berlin', async () => {
      const term = 'berlin';
      const response = await loadWeatherByTerm(term);
      expect(response.type).toBe(LOAD_WEATHER_SUCCESS);
      expect(response.payload!.term).toBe(term);
      expect(response.payload!.data!.name).toBe('Berlin');
    });

    it('returns error with the payload data', async () => {
      const term = 'xxx';
      const expected = {
        type: LOAD_WEATHER_ERROR,
        payload: { term }
      };
      expect(await loadWeatherByTerm(term)).toMatchObject(expected);
    });
  });
});

// Reducer
describe('Reducer', () => {
  it('updates the state when `LOAD_WEATHER_REQUEST` is dispached', () => {
    const expected = { data: {}, feedback: '', isLoading: true, term: '' }
    expect(reducer(undefined, showLoading())).toEqual(expected);
  });
});
