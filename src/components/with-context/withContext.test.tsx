import React, { useReducer } from 'react';
import { mount } from 'enzyme';
import withContext from './withContext';
import { INITIAL_STATE, reducer, Context } from '../../store';

// Mocking components with Context
const Component = (props: any) => <div>component</div>;
const ComponentWithContext = withContext(Component);
const App = () => {
  const [state, dispatch] = useReducer(reducer, INITIAL_STATE);
  return (
    <Context.Provider value={{ state, dispatch }}>
      <ComponentWithContext myProp="my-prop" />
    </Context.Provider>
  );
};

describe('withContext()', () => {
  const props = mount(<App />).find('Component').props();

  it('includes the `state` context as a property', () => {
    expect(props).toHaveProperty('state');
  });

  it('includes the `dispatch` context as a property', () => {
    expect(props).toHaveProperty('dispatch');
  });

  it('includes my custom property `myProp`', () => {
    expect(props).toHaveProperty('myProp');
  });
});