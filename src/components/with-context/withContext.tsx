import React, { useContext } from 'react';
import { Context } from '../../store';

const withContext = (Component: React.ComponentType) => (props: any) => {
  const context = useContext(Context);
  return <Component {...props} {...context} />;
};

export default withContext;
