import App from './app';
import withContext from '../with-context';

export default withContext(App);
