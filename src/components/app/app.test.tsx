import React from 'react';
import ReactDOM from 'react-dom';
import { mount } from 'enzyme';
import App from './app';

describe('App', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders with CardList component', () => {
    const app = mount(<App />);
    expect(app.find('CardList')).toHaveLength(1);
    // expect(app.find('CardList').exists()).toBe(true);
  });
});
