import React, { useEffect } from 'react';
import CardList from '../card-list';
import { showLoading, loadWeatherByTerm, IContextProp } from '../../store';

const App = ({ dispatch }: IContextProp) => {
  const firstLoadByTerm = async (term: string) => {
    dispatch!(showLoading());
    const data = await loadWeatherByTerm(term);
    dispatch!(data);
  };

  useEffect(() => {
    firstLoadByTerm('berlin');
  }, []);

  return (
    <CardList />
  );
};

export default App;
