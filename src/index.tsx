import React, { useReducer } from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import { INITIAL_STATE, reducer, Context } from './store';
import './assets/styles/global.scss';
import * as serviceWorker from './serviceWorker';

const Main = () => {
  const [state, dispatch] = useReducer(reducer, INITIAL_STATE);
  return (
    <Context.Provider value={{ state, dispatch }}>
      <App tes="test" abc="abcaa" />
    </Context.Provider>
  );
}

ReactDOM.render(<Main />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
