import { objectToArray } from '../utils';

describe('Utils methods:', () => {
  describe('objectToArray()', () => {
    it('converts an object list into an array', () => {
      const obj = { a: { a1: 'a1', a2: 'a2' }, b: { b1: 'b1', b2: 'b2' } };
      const expected = [ { a1: 'a1', a2: 'a2' }, { b1: 'b1', b2: 'b2' } ];
      expect(objectToArray(obj)).toEqual(expected);
    });

    it('converts an empty object into an empty array', () => {
      expect(objectToArray({})).toEqual([]);
    });
  });
});