import { customFetch } from './utils';

// API constants
const URL = 'https://api.openweathermap.org/data/2.5/weather';
const KEY = '?APPID=24312b65699c3bccbcd0fab6eeaf5681';
const UNIT = '&units=metric';
const QUERY = '&q=';

/**
 * Get weather data by city
 * @param {String} term
 * @return {Promise}
 */
const getWeatherByTerm = (term: string): Promise<any> => {
  if (!term) throw Error('The "term" param was not passed!');
  const url = `${URL}${KEY}${UNIT}${QUERY}${term}`;
  return customFetch(url);
};

export default getWeatherByTerm;