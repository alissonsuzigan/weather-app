import { IData } from '../../store';

/**
 * An abstraction to the fetch method
 */
export const customFetch = async (url: string, params: IData = {}): Promise<any> => {
  const response: IData = await fetch(url, params);
  if (response.ok) return Promise.resolve(response);
  return Promise.reject(new Error(response.status));
};

/**
 * Convert object structure into array
 */
export const objectToArray = (obj: IData): Array<any> => {
  return Object.keys(obj).map(item => obj[item]);
};
